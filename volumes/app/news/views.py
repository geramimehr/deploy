from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from .models import Detail, Tag
from .serializers import (
    DetailGetSerializer,
    DetailCreateUpdateDeleteSerializer,
    TagSerializer,
)
from .paginations import NewsPagiantion
from .permissions import IsAdminOrReadOnly

# Create your views here.


class NewsModelViewSet(ModelViewSet):
    """
    This ModelViewSet automatically provides 'psot', 'get', 'patch', 'delete' action  for the Detail model.
    Filters are applied to allow clients to sort the News by 'tags'.news list paginate by NewsPagiantions.

    """

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method in ["POST", "PATCH", "DELETE"]:
            return DetailCreateUpdateDeleteSerializer
        else:
            return DetailGetSerializer

    queryset = Detail.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    pagination_class = NewsPagiantion
    http_method_names = ["get", "post", "patch", "delete"]
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ["tags"]


class TagModelViewSet(ModelViewSet):
    """
    This ModelViewSet automatically provides 'psot', 'get', 'patch', 'delete' action  for the Tag model.
    Filters are applied to allow clients to sort the Tag by 'tags'.
    """

    queryset = Tag.objects.all()
    permission_classes = [IsAdminOrReadOnly]
    serializer_class = TagSerializer
    http_method_names = ["get", "post", "patch", "delete"]
