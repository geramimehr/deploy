from django.urls import path, include
from .views import NewsModelViewSet, TagModelViewSet
from rest_framework.routers import SimpleRouter

news_router = SimpleRouter()
news_router.register("news", NewsModelViewSet)

tag_router = SimpleRouter()
tag_router.register("tag", TagModelViewSet)

urlpatterns = [
    path("", include(news_router.urls), name="news"),
    path("", include(tag_router.urls), name="tag"),
]
