from rest_framework.pagination import PageNumberPagination

class NewsPagiantion(PageNumberPagination):
    page_size = 12
    page_query_param = "page_number"
    page_size_query_param = 'page_size'