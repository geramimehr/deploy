from rest_framework import  serializers
from .models import Detail, Tag

class DetailGetSerializer(serializers.ModelSerializer):
    tags = serializers.SerializerMethodField()
    class Meta:
        model = Detail
        fields = ['title', 'news_text', 'resource', 'tags'] 
    
    def get_tags(self, obj):
        new_tags = []
        tags = obj.tags.all()
        for tag in tags:
            new_tags.append(tag.name)
        return new_tags


class DetailCreateUpdateDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Detail
        fields = ['title', 'news_text', 'resource', 'tags'] 
  

class TagSerializer(serializers.ModelSerializer):
    id=serializers.IntegerField(read_only=True)
    class Meta:
        model = Tag
        fields = ['id', 'name']

      
    