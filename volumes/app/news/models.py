from django.db import models

# Create your models here.

class Tag(models.Model):
    name= models.CharField(max_length = 125) 

    def __str__(self) -> str:
        return self.name
    

class Detail(models.Model):
    title = models.CharField(max_length = 125)
    news_text = models.TextField()
    resource = models.TextField()
    tags = models.ManyToManyField(Tag,related_name="news",blank=True)
    
    def __str__(self) -> str:
        return self.title


