from django.contrib.auth.models import User
from rest_framework.test import APITestCase
import base64
from rest_framework import status
from news.models import Detail, Tag

# Create your tests here.


class TagAPI(APITestCase):

    """
    this tests for tag API
    """

    fixtures = ["tags.json", "news.json"]

    def setUp(self) -> None:
        self.list_ = self.psot_ = "/api/tag/"
        self.delete_ = self.update_ = self.get_ = "/api/tag/{id}/"
        self.new_tag = {"name": "sports"}
        self.result_new_tag = {"id": 4, "name": "sports"}
        self.update_tag = {"name": "new_mobile"}
        self.resuslt_update_tag = {"id": 1, "name": "new_mobile"}
        self.superuser = {
            "username": "admin",
            "email": "admin@localhost",
            "password": "admin",
        }
        self.usr = User.objects.create_superuser(**self.superuser)
        self.headers = {
            "HTTP_AUTHORIZATION": "Basic "
            + base64.b64encode(b"admin:admin").decode("ascii")
        }

    def test_get_list(self):
        response = self.client.get(self.list_)
        self.assertEqual(
            response.json(),
            [
                {"id": 1, "name": "mobile"},
                {"id": 2, "name": "tech"},
                {"id": 3, "name": "iran"},
            ],
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_donot_permission(self):
        response = self.client.post(self.psot_, self.new_tag)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post(self):
        response = self.client.post(self.psot_, self.new_tag, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json(), self.result_new_tag)

    def test_update_donot_permission(self):
        response = self.client.patch(
            self.update_.format(id=1),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.client.patch(
            self.update_.format(id=1), self.update_tag, **self.headers
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(self.update_.format(id=1))
        self.assertEqual(response.json(), self.resuslt_update_tag)

    def test_delete_donot_permission(self):
        response = self.client.patch(self.update_.format(id=1))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.client.delete(self.update_.format(id=1), **self.headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(self.update_.format(id=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class DetailAPI(APITestCase):

    """
    this tests for Detail API
    """

    fixtures = ["tags.json", "news.json"]

    def setUp(self) -> None:
        self.list_ = self.psot_ = "/api/news/"
        self.delete_ = self.update_ = self.get_ = "/api/news/{id}/"

        self.new_news = {
            "title": "news4",
            "news_text": "text4",
            "resource": "resource4",
            "tags": [1],
        }

        self.update_news = {"title": "new_news"}
        self.result_update_news = {
            "title": "new_news",
            "news_text": "text1",
            "resource": "resource1",
            "tags": ["iran"],
        }

        self.superuser = {
            "username": "admin",
            "email": "admin@localhost",
            "password": "admin",
        }

        self.usr = User.objects.create_superuser(**self.superuser)
        self.headers = {
            "HTTP_AUTHORIZATION": "Basic "
            + base64.b64encode(b"admin:admin").decode("ascii")
        }

    def test_get_list(self):
        response = self.client.get(self.list_)
        self.assertEqual(
            response.json()["results"],
            [
                {
                    "title": "news1",
                    "news_text": "text1",
                    "resource": "resource1",
                    "tags": ["iran"],
                },
                {
                    "title": "news2",
                    "news_text": "text2",
                    "resource": "resource2",
                    "tags": ["mobile"],
                },
                {
                    "title": "news3",
                    "news_text": "text3",
                    "resource": "resource3",
                    "tags": ["mobile", "tech"],
                },
            ],
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_donot_permission(self):
        response = self.client.post(self.psot_, self.new_news)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_post(self):
        response = self.client.post(self.psot_, self.new_news, **self.headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json(), self.new_news)

    def test_update_donot_permission(self):
        response = self.client.patch(
            self.update_.format(id=1),
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update(self):
        response = self.client.patch(
            self.update_.format(id=1), self.update_news, **self.headers
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get(self.update_.format(id=1))
        self.assertEqual(response.json(), self.result_update_news)

    def test_delete_donot_permission(self):
        response = self.client.patch(self.update_.format(id=1))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete(self):
        response = self.client.delete(self.update_.format(id=1), **self.headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(self.update_.format(id=1))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
