from django.test import TestCase
from news.models import Detail, Tag

# Create your tests here.


class DetailTest(TestCase):
    """
    test for Detail model
    """

    fixtures = ["tags.json", "news.json"]

    def setUp(self) -> None:
        pass

    def test_str_method(self):
        news1 = Detail.objects.get(title="news1")
        news2 = Detail.objects.get(title="news2")
        news3 = Detail.objects.get(title="news3")
        self.assertEqual(news1.__str__(), "news1")
        self.assertEqual(news2.__str__(), "news2")
        self.assertEqual(news3.__str__(), "news3")

    def test_create_news(self):
        new_news = {
            "title": "news4",
            "news_text": "text4",
            "resource": "resource4",
        }
        obj_news = Detail.objects.create(**new_news)
        obj_news.tags.set([1, 3])
        obj_news.save()
        self.assertNotEquals(obj_news, None)
        self.assertEquals(obj_news.__str__(), "news4")
        self.assertEquals(obj_news.news_text, "text4")
        self.assertSetEqual({x.pk for x in obj_news.tags.all()}, {3, 1})


class tagTest(TestCase):
    """
    test for Tag model
    """

    fixtures = ["tags.json", "news.json"]

    def setUp(self) -> None:
        pass

    def test_str_method(self):
        tag1 = Tag.objects.get(name="mobile")
        tag2 = Tag.objects.get(name="tech")
        tag3 = Tag.objects.get(name="iran")
        self.assertEqual(tag1.__str__(), "mobile")
        self.assertEqual(tag2.__str__(), "tech")
        self.assertEqual(tag3.__str__(), "iran")

    def test_create_tag(self):
        new_tag = {
            "name": "tag4",
        }
        obj_news = Tag.objects.create(**new_tag)
        self.assertNotEquals(obj_news, None)
        self.assertEquals(obj_news.__str__(), "tag4")

    def test_return_reverse_relations(self):
        tag = Tag.objects.get(pk=1)
        self.assertSetEqual({x.pk for x in tag.news.all()}, {3, 2})
